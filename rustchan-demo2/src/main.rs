use std::thread;
use std::sync::mpsc;
fn main() {
    let (tx, rx) = mpsc::channel();
    let v: Vec<u32> = (0..10).collect();
    thread::spawn(move || {
        for val in v {
            println!("sent: {}", val);
            tx.send(val).unwrap();
        }
    });
    for val in rx {
        println!("got {}", val);
    }
}
